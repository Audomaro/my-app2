import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { TareasComponent } from './components/tareas/tareas.component';
import { E400Component } from './components/e400/e400.component';
import { FormTareaComponent } from './components/form-tarea/form-tarea.component';
import { FormTareaRComponent } from './components/form-tarea-r/form-tarea-r.component';

const routes: Routes = [{
  path: 'inicio',
  component: InicioComponent,
}, {
  path: 'tareas',
  component: TareasComponent,
}, {
  path: 'tareas/:id',
  component: FormTareaComponent,
}, {
  path: 'tareas-r/:id',
  component: FormTareaRComponent,
}, {
  path: '400',
  component: E400Component
}, {
  path: '',
  redirectTo: 'inicio',
  pathMatch: 'full'
}, {
  path: '**',
  redirectTo: '400'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
