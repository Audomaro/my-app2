import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { FormTareaComponent } from './components/form-tarea/form-tarea.component';
import { NavegacionComponent } from './components/navegacion/navegacion.component';
import { TareasComponent } from './components/tareas/tareas.component';
import { E400Component } from './components/e400/e400.component';
import { TruncarPipe } from './pipes/truncar.pipe';
import { FormTareaRComponent } from './components/form-tarea-r/form-tarea-r.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    FormTareaComponent,
    NavegacionComponent,
    TareasComponent,
    E400Component,
    TruncarPipe,
    FormTareaRComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
