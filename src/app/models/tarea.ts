export class Tarea {
  public id: number;
  public nombre: string;
  public descripcion: string;

  public terminada: boolean;

  constructor() {
    this.id = 0;
    this.nombre = '';
    this.descripcion = '';
    this.terminada = false;
  }
}
