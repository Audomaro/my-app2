import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncar'
})
export class TruncarPipe implements PipeTransform {

  transform(cadena: any, limitar: string): any {
    const cortar = parseInt(limitar, 10);
    return (cadena && cadena.length) > limitar ? cadena.substring(0, limitar) + '...' : cadena;
  }

}
