import { Injectable } from '@angular/core';

import { Tarea } from '../models/tarea';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TareasService {

  public tareasTotales$ = new BehaviorSubject(0);
  private url = 'http://localhost/webapi/api/Tareas';

  private requestOptions = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  };
  constructor(private httpClient: HttpClient) { }

  public listar(): Observable<Tarea[]> {
    return this.httpClient.get<Tarea[]>(this.url, this.requestOptions);
  }

  public obtener(id: number): Observable<Tarea> {
    return this.httpClient.get<Tarea>(`${this.url}/${id}`, this.requestOptions);
  }

  public agregar(tarea: Tarea): Observable<string> {
    return this.httpClient.post<string>(this.url, tarea, this.requestOptions);
  }

  public actulizar(tarea: Tarea): Observable<string> {
    return this.httpClient.put<string>(this.url, tarea, this.requestOptions);
  }

  public tareasTotales(): Observable<number> {
    return this.tareasTotales$.asObservable();
  }
}
