import { Component, OnInit } from '@angular/core';
import { TareasService } from 'src/app/services/tareas.service';

@Component({
  selector: 'app-navegacion',
  templateUrl: './navegacion.component.html',
  styleUrls: ['./navegacion.component.css']
})
export class NavegacionComponent implements OnInit {

  public menu: { nombre: string, ruta: string }[];

  public totalTareas: number;

  constructor(private tareasService: TareasService) {
    this.menu = [{
      nombre: 'inicio',
      ruta: ''
    }, {
      nombre: 'Tareas',
      ruta: '/tareas',
    }];
  }

  ngOnInit() {
    this.tareasService.tareasTotales().subscribe( total => {
      this.totalTareas = total;
    });
  }
}
