import { Component, OnInit } from '@angular/core';
import { Tarea } from 'src/app/models/tarea';
import { ActivatedRoute, Router } from '@angular/router';
import { TareasService } from 'src/app/services/tareas.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-form-tarea',
  templateUrl: './form-tarea.component.html',
  styleUrls: ['./form-tarea.component.css']
})
export class FormTareaComponent implements OnInit {

  public tarea: Tarea;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private tareaServices: TareasService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((param: any) => {
      const id = parseInt(param.id, 10);
      this.tarea = new Tarea();

      if (id !== 0) {
        this.tareaServices.obtener(id).subscribe(tarea => {
          this.tarea = tarea;
        });
      }
    });
  }

  public guardar(e: any): void {
    let peticion: Observable<any>;

    if (!this.tarea.id) {
      peticion = this.tareaServices.agregar(this.tarea);
    } else {
      peticion = this.tareaServices.actulizar(this.tarea);
    }

    peticion.subscribe(res => {
      this.router.navigate(['tareas']);
    });
  }

  public cancelar(): void {
    this.router.navigate(['tareas']);
  }
}
