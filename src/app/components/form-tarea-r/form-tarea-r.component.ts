import { Component, OnInit, DoCheck } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TareasService } from 'src/app/services/tareas.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form-tarea-r',
  templateUrl: './form-tarea-r.component.html',
  styleUrls: ['./form-tarea-r.component.css']
})
export class FormTareaRComponent implements OnInit {
  public tareaForm = new FormGroup({
    id: new FormControl(0),
    nombre: new FormControl('12', [Validators.required]),
    descripcion: new FormControl('23'),
  });

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private tareaServices: TareasService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((param: any) => {
      this.tareaServices.obtener(parseInt(param.id, 10)).subscribe(tarea => {
        this.tareaForm.controls['id'].setValue(tarea.id);
        this.tareaForm.controls['nombre'].setValue(tarea.nombre);
        this.tareaForm.controls['descripcion'].setValue(tarea.descripcion);
      });
    });
  }

  public guardar(e: any): void {
    if (!this.tareaForm.get('id').value) {
      this.tareaServices.agregar(this.tareaForm.value);
    } else {
      this.tareaServices.actulizar(this.tareaForm.value);
    }

    this.cancelar();
  }

  public cancelar(): void {
    this.router.navigate(['tareas']);
  }

}
