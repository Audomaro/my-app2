import { Component, OnInit } from '@angular/core';
import { Tarea } from 'src/app/models/tarea';
import { TareasService } from 'src/app/services/tareas.service';

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.css']
})
export class TareasComponent implements OnInit {
  public tareas: Tarea[];
  constructor(private tareasService: TareasService) { }

  ngOnInit() {
    this.tareasService.listar().subscribe(tareas => {
      this.tareas = tareas;
      this.tareasService.tareasTotales$.next(tareas.length);
    });
  }

  public marcar(tarea: Tarea): void {
    tarea.terminada = !tarea.terminada;
    this.tareasService.actulizar(tarea);
  }
}
